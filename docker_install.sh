#!/bin/bash

# update the repos
sudo apt-get update 

# install docker
sudo apt install docker.io

# Create the docker group
sudo groupadd docker

# Add your user to the docker group
sudo usermod -aG docker $USER

# logout & again login as the user
su - ${USER}